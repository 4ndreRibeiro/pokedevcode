import java.util.*

enum class Brawlers(val id: Int){TARA(1), CORVO(2), BEA(3), DYNAMIKE(4)}

fun main(args: Array<String>){
    println("Escolha um Brawler!!")

    for((index, p) in  Brawlers.values().withIndex()){
        println("${index + 1}) $p")
    }
    val scanner = Scanner(System.'in')
    val opcao = scanner.nextInt()

    val brawlers: Brawler = gerarBrawler(opcao)
    mostrarDados(brawlers)

    val random = Random()
    val numeroAleatorio = 1 + random.nextInt(4)
    val BrawlersBatalha: Brawler = gerarBrawler(numeroAleatorio)

    //println("Um ${BrawlersBatalha.nome} HP:${brawlers.hp} | ${}")
}

fun mostrarDados(brawler: Brawler){
    println("Um escolhido ${brawler.nome}\nHP:${brawler.hp}\nATAQUE:${brawler.ataque}\nDEFESA:${brawler.defesa}")
}

fun gerarBrawler(opcao: Int) : Brawler = when (opcao){
    Brawlers.TARA.id -> Brawler("Tara", 3400,1380, 720, arrayOf(Ataque("Trinca de Tarô", 1380), Ataque("Gravidade", 2400)))
    Brawlers.CORVO.id -> Brawler("Corvo", 2400,960, 820, arrayOf(Ataque("Punhal", 960), Ataque("Rasante", 3000)))
    Brawlers.BEA.id -> Brawler("Bea", 2400,800, 720, arrayOf(Ataque("Picada", 800), Ataque("Colmeia Robótica", 2200)))
    Brawlers.DYNAMIKE.id -> Brawler("Dynamike", 2800,800, 720, arrayOf(Ataque("Pavio Curto", 800), Ataque("Barril Bombástico", 2200)))
    else -> Brawler("Colette", 3200, 1000, 720, arrayOf(Ataque("Tiro de Tributo", 1200),Ataque("Hora da Coleta",4000)))
}
