import javax.swing.SortOrder

class Brawler (val nome: String, var hp: Int, val ataque: Int, val defesa: Int, val listaAtaques: Array<Ataque>){
    fun obterAtaque(orden: Int) = if (orden < listaAtaques.size) listaAtaques.get(orden) else listaAtaques
}

class Ataque(val  nome: String, val poder: Int)